/**
 * Remove old files, copy front-end ones.
 */

import fs from 'fs-extra';

import childProcess from 'child_process';

// Setup logger




(async () => {
    try {
        // Remove current build
        await remove('./dist/');


        /// these could be replace by esbuild bundle functionality
        // but for now just brute force it
       // await copy('./node_modules/@19labs/gale-wellness/dist/dev/bridge.js', './src/public/scripts/bridge.js')
       // await copy('./node_modules/@19labs/gale-wellness/dist/dev/dom.js', './src/public/scripts/dom.js')

        // Copy front-end files
        await copy('./src/public', './dist/public');
        await copy('./src/views', './dist/views');


        // patch fhir-client
        // removes runtime error with respect ot Headers not defined
        await copy('./src/http-client.js', './node_modules/fhir-kit-client/lib/http-client.js');

        // Copy production env file
        await copy('./src/pre-start/env/production.env', './dist/pre-start/env/production.env');
        // Copy back-end files
        await exec('tsc --build tsconfig.prod.json', './')
    } catch (err) {
        console.log(err);
    }
})();


function remove(loc: string): Promise<void> {
    return new Promise((res, rej) => {
        return fs.remove(loc, (err) => {
            return (!!err ? rej(err) : res());
        });
    });
}


function copy(src: string, dest: string): Promise<void> {
    return new Promise((res, rej) => {
        return fs.copy(src, dest, (err) => {
            return (!!err ? rej(err) : res());
        });
    });
}


function exec(cmd: string, loc: string): Promise<void> {
    return new Promise((res, rej) => {
        return childProcess.exec(cmd, { cwd: loc }, (err, stdout, stderr) => {
            if (!!stdout) {
                console.info(stdout);
            }
            if (!!stderr) {
                console.warn(stderr);
            }
            return (!!err ? rej(err) : res());
        });
    });
}
