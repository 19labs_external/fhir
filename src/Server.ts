import morgan from 'morgan';
import path from 'path';
import helmet from 'helmet';
import express from 'express'
import session from 'express-session'
import {Request,Response,NextFunction} from 'express'
import StatusCodes from 'http-status-codes';
import 'express-async-errors';
import logger from '@shared/logger';
import flash from 'connect-flash'

import BaseRouter from './routes';

const app = express();
const { BAD_REQUEST } = StatusCodes;



/************************************************************************************
 *                              Set basic express settings
 ***********************************************************************************/

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// this session store should be replaced by a production grade session store
// for the purposes of this example it will work fine
var sess = {
    secret: '19labs rocks telehealth',
    cookie: new session.Cookie()
  }
  
  if (app.get('env') === 'production') {
    app.set('trust proxy', 1) // trust first proxy
    sess.cookie.secure = true // serve secure cookies
  }
app.use(session(sess))

app.set('view engine', 'ejs');

// Show routes called in console during development
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// Security
if (process.env.NODE_ENV === 'production') {
    app.use(helmet());
}

// Add APIs
app.use( BaseRouter);

// Print API errors
// eslint-disable-next-line @typescript-eslint/no-unused-vars
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    logger.error(err.message, true);
    return res.status(BAD_REQUEST).json({
        error: err.message,
    });
});



/************************************************************************************
 *                              Serve front-end content
 ***********************************************************************************/

const viewsDir = path.join(__dirname, 'views');
app.set('views', viewsDir);
const staticDir = path.join(__dirname, 'public');
app.use(express.static(staticDir));


// Export express instance
export default app;
