
export interface IUser {
    id: string;
    name: string;
    email: string;
    org:string;
}


class User implements IUser {
    
    id: string;
    name: string;
    email: string;
    org:string;

    constructor(nameOrUser: string | IUser, email?: string, id?: string,org?:string) {
          
        if (typeof nameOrUser === 'string') {
            this.name = nameOrUser;
            this.email = email || '';
            this.id = id || '';
            this.org = org || ''

        } else {
            this.name = nameOrUser.name;
            this.email = nameOrUser.email;
            this.id = nameOrUser.id;
            this.org= nameOrUser.org;
        }
    }
}

export class AuthenticatedUser  {
    user:User;
    token:string;
    constructor(user:User, token:string){
        this.user=user;
        this.token=token;
    }

}

export default User;
