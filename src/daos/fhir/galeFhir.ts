import { BloodPressureMeasurementData,BloodPressureMeasurement, WeightScaleData, MeasurementWithOptionalTimestamp, SensorTypes, PulseOximeterData, PulseOxMeasurement, GlucometerData, GlucometerMeasurement, ThermometerMeasurement, ThermometerMeasurementData } from '@19labs/gale-wellness'
import { Coding, Encounter, Observation, Patient, Quantity } from 'fhir/r4'
import { addObservation, getObservations } from './fhir'


export function toFhirBloodPressure(blood: BloodPressureMeasurementData, encounter: Encounter, patient: Patient): Observation {

  const observation = {
    resourceType: 'Observation',
    status: 'final',
    code: {
      coding: [
        {
          system: "http://loinc.org",
          code: "55417-0",
          display: "Short Blood Pressure panel"
        }
      ],
    },
    subject: {
      reference: "Patient/" + patient.id
    },
    encounter: {
      reference: "Encounter/" + encounter.id
    },
    effectiveDateTime: new Date(blood.timecomparitor).toISOString(),
    component: [
      {
        code: {
          coding: [
            {
              system: "http://loinc.org",
              code: "8480-6",
              display: "Systolic blood pressure"
            }
          ],
          text: "Systolic blood pressure"
        },
        valueQuantity: {
          value: blood.highpressure,
          unit: "mmHg",
          system: "http://unitsofmeasure.org",
          code: "mm[Hg]"
        }
      },
      {
        code: {
          coding: [
            {
              system: "http://loinc.org",
              code: "8462-4",
              display: "Diastolic blood pressure"
            }
          ],
          text: "Diastolic blood pressure"
        },
        valueQuantity: {
          value: blood.lowpressure,
          unit: "mmHg",
          system: "http://unitsofmeasure.org",
          code: "mm[Hg]"
        }
      },
      {
        code: {
          coding: [
            {
              system: "http://loinc.org",
              code: "8867-4",
              display: "Heart rate"
            }
          ],
          text: "Heart rate"
        },
        valueQuantity: {
          value: blood.heartrate,
          unit: "beats/minute",
          system: "http://unitsofmeasure.org",
          code: "/min"
        }
      }
    ]
  }
  return observation as Observation
}

function getQuantity(observation: Observation, code: string): Quantity | undefined {
  const component = observation.component || []
  for (const entry of component) {
    const codes = entry.code
    const codings = codes.coding || []
    for (const codev of codings) {
      if (codev.code === code) {
        return entry.valueQuantity
      }
    }
  }
  return undefined
}


export function toBloodPressure(observation: Observation): BloodPressureMeasurement {
  const obdate = new Date(observation.effectiveDateTime || '')
  const date = obdate.getFullYear() + '-' + (obdate.getMonth() + 1) + '-' + obdate.getDate()
  const time = obdate.getHours() + ":" + obdate.getMinutes()

  const heartrate = getQuantity(observation, "8867-4");
  const systolic = getQuantity(observation,"8480-6");
  const diastolic = getQuantity(observation,"8462-4");

  const data = {
    pulse: ''+heartrate?.value,
    lowPressure: ''+diastolic?.value,
    highPressure: ''+systolic?.value,
    time: time,
    timestamp: date + ' ' + time,
    timecomparitor: obdate.getTime(),
    type: "Blood Pressure Monitor"
  }
  return data as BloodPressureMeasurement
}

export function toGaleMeasurments(observations:Observation[]):MeasurementWithOptionalTimestamp[]{
  var galeMeasurments:MeasurementWithOptionalTimestamp[] = []
  for (const observation of observations){
    const code = observation.code;
    const codings = code.coding || [];
    for (const value of codings){
        switch (value.code) {
            case "29463-7": {
                const me = toWeight(observation)
                galeMeasurments.push(me)
                break;
            }
            case "8310-5": {
              const me = toTempurature(observation)
              galeMeasurments.push(me)
              break;
          }
            case "55417-0":{
                const me = toBloodPressure(observation)
                galeMeasurments.push(me)
                break;
            }
            case "59408-5":{
              const me = toPulseOx(observation)
              galeMeasurments.push(me)
              break;
          }
          case "15074-8":{
            const me = toGlucose(observation)
            galeMeasurments.push(me)
            break;
        }
        }
    }
  }
  return galeMeasurments
}


export function toObservation(galeMeasurment: any, encounter: Encounter, patient: Patient): Observation | undefined {

  switch (galeMeasurment.type) {
    case "Weight Scale": {
      return toFhirWeight(galeMeasurment, encounter, patient);
      break;
    }
    case "Thermometer": {
      return toFhirTemperature(galeMeasurment,encounter,patient)
      break;
    }
    case "Glucometer": {
      return toFhirGlucose(galeMeasurment,encounter,patient)
      break;
    }
    case "Blood Pressure Monitor": {
      return toFhirBloodPressure(galeMeasurment,encounter,patient)
      break;
    } case "Pulse Oximeter": {
      return toFhirPulseOx(galeMeasurment,encounter,patient)
      break;
    }
  }
  return undefined


}
export function toWeight(observation: Observation): WeightScaleData {
  const obdate = new Date(observation.effectiveDateTime || '')
  const date = obdate.getFullYear() + '-' + (obdate.getMonth() + 1) + '-' + obdate.getDate()
  const time = obdate.getHours() + ":" + obdate.getMinutes()

  const data = {
    weight: observation.valueQuantity?.value,
    unit: observation.valueQuantity?.unit,
    time: time,
    timestamp: date + ' ' + time,
    timecomparitor: obdate.getTime(),
    type: "Weight Scale"
  }
  return data as WeightScaleData
}

export function toFhirWeight(weight: WeightScaleData, encounter: Encounter, patient: Patient): Observation {
  const observation = {
    resourceType: 'Observation',
    status: 'final',
    code: {
      coding: [
        {
          "system": "http://loinc.org",
          "code": "29463-7",
          "display": "Body Weight"
        }
      ]
    },
    subject: {
      reference: "Patient/" + patient.id
    },
    encounter: {
      reference: "Encounter/" + encounter.id
    },
    effectiveDateTime: new Date(weight.timecomparitor).toISOString(),
    valueQuantity: {
      value: weight.weight,
      unit: weight.unit,
      system: "http://unitsofmeasure.org",
      code: weight.unit
    }
  }

  return observation as Observation

}

export function toPulseOx(observation: Observation): PulseOxMeasurement {
  const obdate = new Date(observation.effectiveDateTime || '')
  const date = obdate.getFullYear() + '-' + (obdate.getMonth() + 1) + '-' + obdate.getDate()
  const time = obdate.getHours() + ":" + obdate.getMinutes()

  const oxygen = getQuantity(observation,"2708-6")
  const perfusion = getQuantity(observation,"73798-1")
  const pulse = getQuantity(observation,"8867-4")
  const data = {
    oxygen:oxygen?.value ||'',
    perfusion: perfusion?.value||'',
    pulse: pulse?.value||'',
    time: time,
    timestamp: date + ' ' + time,
    timecomparitor: obdate.getTime(),
    type: "Pulse Oximeter"
  }
  return data as PulseOxMeasurement

}

export function toFhirPulseOx(pox: PulseOximeterData, encounter: Encounter, patient: Patient): Observation {

  const observation = {
    resourceType: 'Observation',
    status: 'final',
    code: {
      coding: [
        {
          system: "http://loinc.org",
          code: "59408-5",
          display: "Oxygen saturation in Arterial blood by Pulse oximetry"
        }
      ],
    },
    subject: {
      reference: "Patient/" + patient.id
    },
    encounter: {
      reference: "Encounter/" + encounter.id
    },
    effectiveDateTime: new Date(pox.timecomparitor).toISOString(),
    component: [
      {
        code: {
          coding: [
            {
              system: "http://loinc.org",
              code: "2708-6",
              display: "Oxygen saturation in Arterial blood"
            }
          ],
          text: "Oxygen saturation in Arterial blood"
        },
        valueQuantity: {
          value: pox.oxygen,
          unit: "%",
          system: "http://unitsofmeasure.org",
          code: "%"
        }
      },
      {
        code: {
          coding: [
            {
              system: "http://loinc.org",
              code: "73798-1",
              display: "Perfusion index Blood Preductal Pulse oximetry"
            }
          ],
          text: "Perfusion index Blood Preductal Pulse oximetry"
        },
        valueQuantity: {
          value: pox.PI,
          unit: "%",
          system: "http://unitsofmeasure.org",
          code: "%"
        }
      },
      {
        code: {
          coding: [
            {
              system: "http://loinc.org",
              code: "8867-4",
              display: "Heart rate"
            }
          ],
          text: "Heart rate"
        },
        valueQuantity: {
          value: pox.pulse,
          unit: "beats/minute",
          system: "http://unitsofmeasure.org",
          code: "/min"
        }
      }
    ]
  }
  return observation as Observation
}

export function toGlucose(observation: Observation):GlucometerMeasurement{
  const obdate = new Date(observation.effectiveDateTime || '')
  const date = obdate.getFullYear() + '-' + (obdate.getMonth() + 1) + '-' + obdate.getDate()
  const time = obdate.getHours() + ":" + obdate.getMinutes()

  const data = {
    bloodSugar:observation.valueQuantity?.value ||'',
    unit: observation.valueQuantity?.unit ||'',
    time: time,
    timestamp: date + ' ' + time,
    timecomparitor: obdate.getTime(),
    type: "Glucometer"
  }
  return data as GlucometerMeasurement
}

export function toFhirGlucose(glucose: GlucometerData, encounter: Encounter, patient: Patient): Observation {

  var units = "mmol/L"
  if (glucose.unit === 0){
      units = "mg/dL"
  }
  const observation = {
    resourceType: 'Observation',
    status: 'final',
    code: {
      coding: [
        {
          "system": "http://loinc.org",
          "code": "15074-8",
          "display": "Glucose [Moles/volume] in Blood"
        }
      ]
    },
    subject: {
      reference: "Patient/" + patient.id
    },
    encounter: {
      reference: "Encounter/" + encounter.id
    },
    effectiveDateTime: new Date(glucose.timecomparitor).toISOString(),
    valueQuantity: {
      value: parseFloat(glucose.glucometerData),
      unit: units,
      system: "http://unitsofmeasure.org",
      code: units
    }
  }

  return observation as Observation

}

export function toTempurature(observation: Observation): ThermometerMeasurement{
  const obdate = new Date(observation.effectiveDateTime || '')
  const date = obdate.getFullYear() + '-' + (obdate.getMonth() + 1) + '-' + obdate.getDate()
  const time = obdate.getHours() + ":" + obdate.getMinutes()
  var tempUnit = 'fahrenheit'
  if (observation.valueQuantity?.unit === 'c'){
      tempUnit = 'celsius'
  }
  const data = {
    temperature: observation.valueQuantity?.value || '',
    unit: tempUnit,
    time: time,
    timestamp: date + ' ' + time,
    timecomparitor: obdate.getTime(),
    type: "Thermometer"
  }
  return data as ThermometerMeasurement
}

export function toFhirTemperature(temperature: ThermometerMeasurementData, encounter: Encounter, patient: Patient): Observation {

  const temperatureString = temperature.temperature;
  const temp = temperatureString.substring(0,temperatureString.length-2)
  const unit = temperatureString.substring(temperatureString.length-1,temperatureString.length)
  var code = '[degF]'
  if (unit === 'c'){
    code="cel"
  }
  const observation = {
    resourceType: 'Observation',
    status: 'final',
    code: {
      coding: [
        {
          "system": "http://loinc.org",
          "code": "8310-5",
          "display": "Body temperature"
        }
      ]
    },
    subject: {
      reference: "Patient/" + patient.id
    },
    encounter: {
      reference: "Encounter/" + encounter.id
    },
    effectiveDateTime: new Date(temperature.timecomparitor).toISOString(),
    valueQuantity: {
      value: parseFloat(temp),
      unit: unit,
      system: "http://unitsofmeasure.org",
      code: code
    }
  }

  return observation as Observation

}