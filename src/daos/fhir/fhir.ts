import logger from '@shared/logger'
import Client from 'fhir-kit-client'
import { Observation, Bundle, BundleEntry, Patient, Parameters, HumanName, OperationOutcome, OperationOutcomeIssue, Practitioner, Encounter } from 'fhir/r4'
import {} from './galeFhir'


const apiKey = "iXvSFamShx8bg9vfEfADc9PqYOtjYNf04TMGjFe8"
const baseUrl = "https://tp9gp9kam4.execute-api.us-east-1.amazonaws.com/dev/tenant/"


export async function searchPatients(token: string, name?: string, gender?: string): Promise<Patient[]> {
  const options = {
    baseUrl: baseUrl + '19labs',
    customHeaders: {
      'x-api-key': apiKey
    },
    bearerToken: token,
  };
  const searchParams: any = {
  }
  if (typeof gender !== 'undefined') {
    searchParams.gender = gender
  }
  if (typeof name !== 'undefined') {
    searchParams.name = name;
  }
  const client = new Client(options);
    let response = await client.search({ resourceType: 'Patient', searchParams: searchParams })
    let bundle: Bundle = response as Bundle;
    let patients: Patient[] = []
    var patientname: HumanName | undefined
    for (let entry of bundle.entry || []) {
      let patient = entry.resource as Patient
      patients.push(patient)
      if (typeof patient.name !== 'undefined') {
        patientname = patient.name[0]

      };

      logger.info("patient:" + patientname?.given + " " + patientname?.family + " " + patient.gender + " " + patient.birthDate + " " + patient.id + " " + patient.identifier)
    }
    return patients;
  

}

export async function getPatient(token: string, id: string): Promise<Patient | undefined> {
  const options = {
    baseUrl: baseUrl + '19labs',
    customHeaders: {
      'x-api-key': apiKey
    },
    bearerToken: token,
  };
  const client = new Client(options);
  logger.info("find patient " + id)
  
    let response = await client.read({ resourceType: 'Patient', id: id });
    if (response.resourceType === 'Patient') {
      console.log("getPatient" + response);
      logger.info('get patient response:' + response);
      return response as Patient
    }
  
  return undefined
}

export async function addPatient(token: string, familyName: string, given: string, dob: string, gender: string) {
  const options = {
    baseUrl: baseUrl + '19labs',
    customHeaders: {
      'x-api-key': apiKey
    },
    bearerToken: token,
  };

  const newPatient = {
    resourceType: 'Patient',
    active: true,
    name: [{ use: 'official', family: familyName, given: given.split(' ') }],
    gender: gender,
    birthDate: dob,
  }
  const client = new Client(options);
  let response = await client.create({
    resourceType: 'Patient',
    body: newPatient,
  })
  return response as Patient
}


export async function removePatient(token: string, patient: Patient): Promise<boolean> {
  const options = {
    baseUrl: baseUrl + '19labs',
    customHeaders: {
      'x-api-key': apiKey
    },
    bearerToken: token,
  };
  const client = new Client(options);
  try {

    let response = await client.delete({ resourceType: 'Patient', id: patient.id as string });
    if (response.resourceType === "OperationOutcome") {
      let outcome = response as OperationOutcome
      for (let entry of outcome.issue || []) {
        if (entry.severity === "information") {
          if (entry.diagnostics) {
            if (entry.diagnostics.indexOf("Success") >= 0) {
              return true
            }
          }
        } else {
          logger.error("serverity:" + entry.severity + " code:" + entry.code + " diagnostic:" + entry.diagnostics)
          return false
        }
      }

    }
  } catch (error) {
    const anyError: any = error as any
    if (error as any)
      if (anyError.response.status === 404) {
        logger.info("not found ")
      } else {
        logger.error(JSON.stringify(error))
      }

  }
  return false;
}

export async function addPractitioner(token: string, family: string, given: string) {
  const options = {
    baseUrl: baseUrl + '19labs',
    customHeaders: {
      'x-api-key': apiKey
    },
    bearerToken: token,
  };

  const newPractitioner = {
    resourceType: 'Practitioner',
    active: true,
    name: [{ use: 'official', family: family, given: given.split(' ') }],
  }
  const client = new Client(options);
  let response = await client.create({
    resourceType: 'Practitioner',
    body: newPractitioner,
  })
  return response as Practitioner
}


export async function addEncounter(token: string, patient: Patient, status: string) :Promise<Encounter> {
  const options = {
    baseUrl: baseUrl + '19labs',
    customHeaders: {
      'x-api-key': apiKey
    },
    bearerToken: token,
  };

  const newEncounter = {
    resourceType: 'Encounter',
    status: status,
    subject: {
      reference: "Patient/" + patient.id
    },
    class: {
      system: "http://terminology.hl7.org/CodeSystem/v3-ActCode",
      code: "VR",
      display: "virtual"
    }

  }
  const client = new Client(options);
  let response = await client.create({
    resourceType: 'Encounter',
    body: newEncounter,
  })
  return response as Encounter
}

export async function updateEncounter(token: string, encounter: Encounter, status: ('planned'|'arrived'|'triaged'|'in-progress'|'onleave'|'finished'|'cancelled'|'entered-in-error'|'unknown')) :Promise<Encounter>{
  const options = {
    baseUrl: baseUrl + '19labs',
    customHeaders: {
      'x-api-key': apiKey
    },
    bearerToken: token,
  };

  encounter.status=status
  const client = new Client(options);
  let response = await client.update({
    resourceType: 'Encounter',
    id: encounter.id as string,
    body: encounter,
  });
  return response as Encounter
}

export async function getEncounter(token: string, id: string): Promise<Encounter | undefined> {
  const options = {
    baseUrl: baseUrl + '19labs',
    customHeaders: {
      'x-api-key': apiKey
    },
    bearerToken: token,
  };
  const client = new Client(options);
  logger.info("find encounter " + id)
  
    let response = await client.read({ resourceType: 'Encounter', id: id });
    if (response.resourceType === 'Encounter') {
      logger.info('get encounter response:' + JSON.stringify(response));
      return response as Encounter
    }
  
  return undefined
}
export async function historyEncounter(token: string, encounter: Encounter) {
  const options = {
    baseUrl: baseUrl + '19labs',
    customHeaders: {
      'x-api-key': apiKey
    },
    bearerToken: token,
  };

  const client = new Client(options);
  let response = await client.history({
    resourceType: 'Encounter',
    id: encounter.id as string,
  });
 return response 
}



export async function addObservation(token: string, observation:Observation) :Promise<Observation> {
  const options = {
    baseUrl: baseUrl + '19labs',
    customHeaders: {
      'x-api-key': apiKey
    },
    bearerToken: token,
  };

  
  const client = new Client(options);
  let response = await client.create({
    resourceType: 'Observation',
    body: observation,
  })
  return response as Observation
}

export async function getObservations(token: string, patient:Patient) :Promise<Observation[]> {
  const options = {
    baseUrl: baseUrl + '19labs',
    customHeaders: {
      'x-api-key': apiKey
    },
    bearerToken: token,
  };
  const search ={
    resourceType: 'Observation',
    searchParams: { patient: patient.id || ''},
  }

  options.baseUrl= options.baseUrl
  const client = new Client(options);
  let response = await client.resourceSearch(search);
  let bundle: Bundle = response as Bundle;
  let obs: Observation[] = []
  for (let entry of bundle.entry || []) {
    let observation = entry.resource as Observation
    obs.push(observation)
  }
  return obs as Observation[]
}

export async function removeObservation(token: string, observation: Observation): Promise<boolean> {
  const options = {
    baseUrl: baseUrl + '19labs',
    customHeaders: {
      'x-api-key': apiKey
    },
    bearerToken: token,
  };
  const client = new Client(options);
  try {

    let response = await client.delete({ resourceType: 'Observation', id: observation.id as string });
    if (response.resourceType === "OperationOutcome") {
      let outcome = response as OperationOutcome
      for (let entry of outcome.issue || []) {
        if (entry.severity === "information") {
          if (entry.diagnostics) {
            if (entry.diagnostics.indexOf("Success") >= 0) {
              return true
            }
          }
        } else {
          logger.error("serverity:" + entry.severity + " code:" + entry.code + " diagnostic:" + entry.diagnostics)
          return false
        }
      }

    }
  } catch (error) {
    const anyError: any = error as any
    if (error as any)
      if (anyError.response.status === 404) {
        logger.info("not found ")
      } else {
        logger.error(JSON.stringify(error))
      }

  }
  return false;
}




