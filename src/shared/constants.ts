// Put shared constants here

export const paramMissingError = 'One or more of the required parameters was missing.';

export const cognitoPool={
  "poolid":"us-east-1_RuC2gdqk0",
  "region":"us-east-1",
  "clientid":"5k8ifcjafvrc4vnblhb76rd1pn"
}

export const fhirApiKey="iXvSFamShx8bg9vfEfADc9PqYOtjYNf04TMGjFe8"
export const galeApiKey=process.env.GALE_API_KEY 
export const startClinicName=process.env.START_CLINIC
