
import logger from '@shared/logger';
import { CognitoUserPool, CognitoUserAttribute, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';
import { paramMissingError, cognitoPool} from '@shared/constants';
import User, { AuthenticatedUser } from '@entities/User';

var poolData = {
    UserPoolId: cognitoPool.poolid, // Your user pool id here
    ClientId: cognitoPool.clientid // Your client id here
};


var currentAuthToken: string;
var email: string
var id: string
var org: string

export async function authenticate(username: string, password: string) {

       const un = username.trim();
       const pwd = password.trim();

    return new Promise<AuthenticatedUser>((resolve, reject) => {
        var authenticationData = {
            Username: un,
            Password: pwd,
        };
        var authenticationDetails = new AuthenticationDetails(authenticationData);
    
        var userPool = new CognitoUserPool(poolData);

        var userData = {
            Username: un,
            Pool: userPool
        };
        var cognitoUser = new CognitoUser(userData);

        logger.info("auth details "+JSON.stringify(authenticationDetails))
        cognitoUser.setAuthenticationFlowType('USER_PASSWORD_AUTH');
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                logger.info("SUCCESS")
                logger.info('access token:' + currentAuthToken);

                // Instantiate aws sdk service objects now that the credentials have been updated.
                // example: var s3 = new AWS.S3();
                currentAuthToken = result.getIdToken().getJwtToken();
                logger.info('access token:' + currentAuthToken);
                cognitoUser.getUserAttributes(function (err, result) {
                    if (err) {
                        alert(err);
                        return;
                    }
                    var given ='';
                    var family = ''
                    if (result) {
                        let i = 0
                        for (i = 0; i < result.length; i++) {
                            logger.info(JSON.stringify(result[i]))

                            const attribute = result[i]
                            logger.info("attribute "+attribute.getName()+" "+attribute.getValue())

                            if (attribute.Name == 'email') {
                                email = attribute.Value
                            }
                            if (attribute.Name == 'given_name') {
                                given += attribute.Value
                            }
                            if (attribute.Name == 'family_name') {
                                family += attribute.Value
                            }

                            if (attribute.Name === 'sub') {
                                id = attribute.Value
                            }
                            if (attribute.Name === 'custom:tenantId') {
                                org = attribute.Value
                            }
                        }
                    }
                    var name= given+' '+family;
                    if (name.length == 1){
                        name = cognitoUser.getUsername()
                    }
                    const user = new User(name,email,id,org)
                    const authenticatedUser = new AuthenticatedUser(user,currentAuthToken)
                    resolve(authenticatedUser)
                });
                
            },

            onFailure: function (err) {
                logger.error(JSON.stringify(err))
                reject(err)
            },

        });
    });

}

