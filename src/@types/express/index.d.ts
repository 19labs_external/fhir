import { IUser } from "@entities/User";

declare module 'express' {
    export interface Request  {
        body: {
            user: IUser
        };
    }
}

declare module 'express-session' {
        interface SessionData {
            user: IUser
            token: string
            patientId: string
            sessionStartTime: number
            historyLoadedTime:number
            encounterId: string
        }
    }
