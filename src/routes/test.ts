import logger from '@shared/logger';
import { response, Router } from 'express';
import { authenticate } from '../shared/auth';
import { searchPatients, addPatient, removePatient, getPatient, addEncounter, updateEncounter, historyEncounter, addObservation, getObservations, removeObservation } from '@daos/fhir/fhir'
import { Bundle, BundleEntry, Patient, Parameters, HumanName } from 'fhir/r4'
import { toBloodPressure, toFhirBloodPressure, toFhirGlucose, toFhirPulseOx, toFhirTemperature, toFhirWeight, toGaleMeasurments, toGlucose, toObservation, toPulseOx, toTempurature } from '@daos/fhir/galeFhir'
import { BloodPressureMeasurementData, GlucometerData, GlucometerMeasurement, PulseOximeterData, PulseOxMeasurement, ThermometerMeasurementData, WeightScaleData } from '@19labs/gale-wellness';


const testRouter = Router();

testRouter.get('/Patients', async (req, res) => {
    if (req.session.token === 'undefined') {
        res.sendStatus(401)
    } else {
        const user = req.session.user
        const token = req.session.token || ' ';
        logger.info('search for patients ' + token)
        try {

            logger.info("search patient by last name name ")
            var patients: Patient[] = await searchPatients(token, "smith")
            logger.info("number of patients" + patients.length)
            if (patients.length != 2) {
                logger.error("Fail should be 2 patients named smih")
            }

            logger.info(" search patients with no parameters")
            patients = await searchPatients(token)
            logger.info("number of patients" + patients.length)
            if (patients.length < 2) {
                logger.error("Fail not enough patients")
            }

            logger.info("search patient by first name ")
            var patients: Patient[] = await searchPatients(token, "emily")
            logger.info("number of patients" + patients.length)
            if (patients.length != 1) {
                logger.error("Fail should be 1 patients named emily")
            }
            logger.info("search patient for female  ")
            patients = await searchPatients(token, undefined, 'female')
            logger.info("number of patients" + patients.length)

            let netpatient = await getPatient(token, patients[0].id || '')
            logger.info("get patient returned" + typeof netpatient + " " + JSON.stringify(netpatient))

            let nopatient = await getPatient(token, 'no possible id')
            logger.info("get patient returned" + typeof netpatient)

            let newpatient = await addPatient(token, "Jones", "Cyril B.", "1949-02-28", 'male')
            logger.info("add patient returned" + typeof newpatient + " " + JSON.stringify(netpatient))

            logger.info(" search patients with no parameters")
            patients = await searchPatients(token)
            logger.info("number of patients" + patients.length)
            if (patients.length < 2) {
                logger.error("Fail not enough patients")
            }

            logger.info("remove newpatient " + newpatient.id)
            let state = await removePatient(token, newpatient)
            logger.info("removed " + state)
            if (state) {
                logger.info("successful deletion")
            } else {
                logger.error("Fail delete test")
            }

            if (await removePatient(token, newpatient)) {
                logger.error("fail double deletion")
            } else {
                logger.info(" susccful double delete")
            }



        } catch (err) {
            logger.error(" catch error " + JSON.stringify(err))
        }
    }
})


testRouter.get('/Encounter', async (req, res) => {
    if (req.session.token === 'undefined') {
        res.sendStatus(401)
    } else {
        const user = req.session.user
        const token = req.session.token || ' ';
        logger.info('search for patients ' + token)
        try {


            logger.info("search patient by first name ")
            var patients: Patient[] = await searchPatients(token, "emily")
            logger.info("number of patients" + patients.length)
            if (patients.length != 1) {
                logger.error("Fail should be 1 patients named emily")
            }

            let netpatient = await getPatient(token, patients[0].id || '')
            logger.info("get patient returned" + typeof netpatient + " " + JSON.stringify(netpatient))
            if (netpatient) {
                let encounter = await addEncounter(token, netpatient, 'arrived')
                logger.info("Encounter:" + JSON.stringify(encounter))

                encounter = await updateEncounter(token, encounter, 'finished')
                logger.info("Updated Encounter:" + JSON.stringify(encounter))

            }


        } catch (err) {
            logger.error(" catch error " + JSON.stringify(err))
        }
    }
})
testRouter.get('/expired', (req, res) => {
    const token = "eyJraWQiOiJvajBuS0gzXC9pUkFMeDdtQmVWXC93ZVhFeDBpQkpiYVlMVmRFM2dvQXVsbmM9IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJmZWYwYTAzMC01OTc2LTRmYzctYTJmYS1lNjVlOTI5OTUzNTgiLCJjb2duaXRvOmdyb3VwcyI6WyJwcmFjdGl0aW9uZXIiXSwiZW1haWxfdmVyaWZpZWQiOnRydWUsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0LTEuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0xX1J1QzJnZHFrMCIsInBob25lX251bWJlcl92ZXJpZmllZCI6dHJ1ZSwiY29nbml0bzp1c2VybmFtZSI6InByYWN0aXRpb25lciIsImdpdmVuX25hbWUiOiJHcmFjaWUiLCJvcmlnaW5fanRpIjoiNjMwMWQ5YTAtZWYxMy00N2FiLWE2NjctYjBhOGJiOGVkMzBiIiwiY3VzdG9tOnRlbmFudElkIjoiMTlsYWJzIiwiYXVkIjoiNWs4aWZjamFmdnJjNHZuYmxoYjc2cmQxcG4iLCJldmVudF9pZCI6IjJjZTZjYzY5LTMxMzYtNGZkZC1hMWU1LWE2OTlhOGU3OGJhMyIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjQwMDMzNjY3LCJwaG9uZV9udW1iZXIiOiIrMTc3ODY3ODAyODgiLCJleHAiOjE2NDAwMzcyNjcsImlhdCI6MTY0MDAzMzY2NywiZmFtaWx5X25hbWUiOiJKb25lcyIsImp0aSI6IjU4MjMxNGZkLTM2NmQtNDE5MC04NzcwLWI0OGJmMGI5ZDE5OSIsImVtYWlsIjoiZ3JhY2llQDE5bGFicy5jb20ifQ.RnP3fZDf0j5DVoDn0w8tWxWLx6bCgU_85JoV7OWopdvvpM5UPeSkbkNOvir67mlSfMTTsp4d7s6eufFc15l4Bk4iXwPEhhQhRn0op-WSCnGZB6Rkp5Bn2wRWPLkbRx9eWe_vH0IvDjpPaZv1K08r4bYqO6_D1nNqNAd1AiHYxCrrHKU2wfTnwn6C3b5KOFZlewzA0qPW1cm-e7e4iSQPCtT2REx9gv_o35TULatsZlFBhmMdPIfpQxQgvjaRsKofNKC24BsFrj9BDuE1YcPW6kJzgL1wFwJidlInxdu84nRNanBuVapIlmh_nWPrrBSMcViyPhveJufWtUucmA4GgQ"
    searchPatients(token, "emily").then((patients) => { }).catch((error) => {
        logger.error("found error " + JSON.stringify(error));
        logger.error("bits " + JSON.stringify(error.response))
        logger.error("code:" + error.response.status + " " + error.response.data.message)
    })

})

testRouter.get('/clean', async (req, res) => {
    if (req.session.token === 'undefined') {
        res.sendStatus(401)
    } else {
        const user = req.session.user
        const token = req.session.token || ' ';

        logger.info('search for patients ' + token )
        try {
          

            logger.info("search patient by first name ")
            var patients: Patient[] = await searchPatients(token, "emily")
            logger.info("number of patients" + patients.length)
            if (patients.length != 1) {
                logger.error("Fail should be 1 patients named emily")
            }

            let patient = await getPatient(token, patients[0].id || '')
            logger.info("get patient returned" + patient?.name)
            if (patient) {
              
                let observations = await getObservations(token, patient)
                logger.info("search observations " + JSON.stringify(observations))

               
                for (const obs of observations){
                     
                     const date = new Date(obs.effectiveDateTime || '');
                     const now = new Date()
                     if ((now.getTime() - date.getTime()) < (24*60*60*1000)){
                         logger.info("remove "+obs.id)
                         const delresp = removeObservation(token,obs)
                         logger.info("delete"+JSON.stringify(delresp))
                     }
                     
                }

            }


        } catch (err) {
            logger.error(" catch error " + JSON.stringify(err))
        }


    }
})

testRouter.get('/Observation', async (req, res) => {
    if (req.session.token === 'undefined') {
        res.sendStatus(401)
    } else {
        const user = req.session.user
        const token = req.session.token || ' ';

        const weight = {
            unit: 'lbs',
            weight: 320,
            timestamp: new Date().toISOString(),
            timecomparitor: new Date().getTime(),
        } as WeightScaleData
      
        const blood ={ 
            heartrate: 55,
            highpressure: 120,
            lowpressure:75,
            timestamp: new Date().toISOString(),
            timecomparitor: new Date().getTime(),
        } as BloodPressureMeasurementData


        const pox = { 
            pulse: 55,
            oxygen: 94,
            PI:2.5,
            timestamp: new Date().toISOString(),
            timecomparitor: new Date().getTime(),
        } as PulseOximeterData

        const sugar = {
            glucometerData: ""+7.1,
            unit: 0,
            timestamp: new Date().toISOString(),
            timecomparitor: new Date().getTime(),
        } as GlucometerData

        const temperature = {
        temperature:"98°F",
        timestamp: new Date().toISOString(),
        timecomparitor: new Date().getTime(),
        } as ThermometerMeasurementData

        logger.info('search for patients ' + token )
        try {
          

            logger.info("search patient by first name "+blood)
            var patients: Patient[] = await searchPatients(token, "emily")
            logger.info("number of patients" + patients.length)
            if (patients.length != 1) {
                logger.error("Fail should be 1 patients named emily")
            }

            let patient = await getPatient(token, patients[0].id || '')
            logger.info("get patient returned" + patient?.name)
            if (patient) {
                let encounter = await addEncounter(token, patient, 'arrived')
                logger.info("Encounter:" + JSON.stringify(encounter))

                var observation = toFhirWeight(weight, encounter, patient)
                logger.info("observation " + JSON.stringify(observation))

                var response = await addObservation(token, observation)
                logger.info("response " + JSON.stringify(response))

                observation = toFhirBloodPressure(blood,encounter,patient)
                response = await addObservation(token, observation)
                logger.info("response " + JSON.stringify(response))

                 const bloodr = toBloodPressure(response)
                logger.info(" blood respons e"+JSON.stringify(bloodr))

                observation = toFhirPulseOx(pox,encounter,patient)
                response = await addObservation(token, observation)
                logger.info("response " + JSON.stringify(response))
                
                 const poxr = toPulseOx(response)
                logger.info(" pulsox response"+JSON.stringify(poxr))
                

                observation = toFhirGlucose(sugar,encounter,patient)
                response = await addObservation(token, observation)
                logger.info("response " + JSON.stringify(response))
                
                const sugarr = toGlucose(response)
                logger.info(" sugar response"+JSON.stringify(sugarr))

                observation = toFhirTemperature(temperature,encounter,patient)
                response = await addObservation(token, observation)
                logger.info("temp response " + JSON.stringify(response))
                
                const tempr = toTempurature(response)
                logger.info(" tempr response"+JSON.stringify(tempr))


                encounter = await updateEncounter(token, encounter, 'finished')
                logger.info("Updated Encounter:" + JSON.stringify(encounter))

                let observations = await getObservations(token, patient)
                logger.info("search observations " + JSON.stringify(observations))

                const galeMeasurments = toGaleMeasurments(observations)

                logger.info("results as gale "+JSON.stringify(galeMeasurments))

            

            }


        } catch (err) {
            logger.error(" catch error " + JSON.stringify(err))
        }


    }
})
export default testRouter
