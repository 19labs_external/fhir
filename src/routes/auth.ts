import logger from '@shared/logger';
import { Router,Request,Response } from 'express';
import { authenticate } from '../shared/auth'
import { galeApiKey } from '@shared/constants';
import error from './index'


export const authRouter = Router();
authRouter.post('/cognito', (req, res) => {
    logger.info(" login user cognito " + req.body.username + " pwd:" + req.body.password);

    authenticate(req.body.username, req.body.password)
        .then((authuser) => {
            req.session.token = authuser.token;
            req.session.user = authuser.user;
            logger.info("Login Success " + JSON.stringify(authuser));
            res.redirect('/home');

        })
        .catch((error) => {
            logger.error(error.message);
            res.render('index',{error,galeApiKey})
        });


})

authRouter.get('/endPatientSession', (req, res) => {
    logger.info("end session")
    if(typeof req.session.patientId !== 'undefined'){
        logger.info("End patient session")
        req.session.patientId = undefined;
    }
    res.redirect('/home');

})

authRouter.get('/logout', (req, res) => {
    logout(req,res);
})

export function logout(req:Request,res:Response){
    req.session.user = undefined;
    req.session.token = undefined;
    req.session.patientId = undefined;
    req.session.sessionStartTime=undefined;
    req.session.encounterId=undefined;
    res.redirect('/');
};

export default authRouter

