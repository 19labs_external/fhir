import logger from '@shared/logger';
import { Router } from 'express';
import { authenticate } from '../shared/auth';
import { getPatient, searchPatients, addPatient, addObservation, addEncounter, getEncounter } from '@daos/fhir/fhir'
import { Patient } from 'fhir/r4';
import { MeasurementWithOptionalTimestamp } from '@19labs/gale-wellness'
import { toObservation } from '@daos/fhir/galeFhir';


const dataRouter = Router();
dataRouter.post('/profile', (req, res) => {
    // add a profile to fhir
    if (typeof req.session.user === 'undefined' || typeof req.session.token === 'undefined') {
        res.redirect('/login')
    } else {
        logger.info(" insert user profile " + JSON.stringify(req.body));
        addPatient(req.session.token, req.body.lastName, req.body.firstName, req.body.dob, req.body.gender)
            .then((patient) => {
                res.redirect('/dashboard/' + patient.id)
            })
            .catch((error) => {
                logger.error("error adding patient " + error.message);
            });

    }

})

dataRouter.post('/summary', async (req, res) => {
    // upload summary data to fhir
    const observations: MeasurementWithOptionalTimestamp[] = req.body
    const token = req.session.token || ''

    let patient = await getPatient(token, req.session.patientId || '')
    logger.info("get patient returned" + patient?.name)
    if (patient && observations.length > 0) {
        let encounter = await getEncounter(token, req.session.encounterId || '');
        logger.info("Encounter:" + JSON.stringify(encounter))
        for (const observation of observations) {
            if (patient) {
                if (encounter) {
                    const fhirobs = toObservation(observation, encounter, patient)
                    logger.info("Observation: " + JSON.stringify(fhirobs))
                    if (fhirobs) {
                        await addObservation(token, fhirobs)
                    }
                }
            }
        }
    }
    req.session.historyLoadedTime = new Date().getTime();
    logger.info("save complete at " + req.session.historyLoadedTime)

    res.sendStatus(200)

})
export default dataRouter

