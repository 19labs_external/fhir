import logger from '@shared/logger';
import { Router } from 'express';
import { authenticate } from '../shared/auth';
import { getObservations, getPatient, searchPatients ,addEncounter} from '@daos/fhir/fhir'
import { MeasurementDataWithTimestamp } from '@19labs/gale-wellness'
import testRouter from './test';
import dataRouter from './data';
import { authRouter, logout } from './auth'
import { Patient, Observation, Encounter} from 'fhir/r4'
import { toBloodPressure, toGaleMeasurments, toWeight } from '@daos/fhir/galeFhir';
import { Console, debug } from 'console';
import { galeApiKey,startClinicName} from '@shared/constants'

// Export the base-router
const baseRouter = Router();
baseRouter.use('/auth', authRouter);
baseRouter.use('/data', dataRouter)
baseRouter.use('/test', testRouter)

var error = {
    code: 0,
    message: ''
}

baseRouter.get('/login', function (req, res) {
    if (typeof req.session.user === 'undefined') {
        res.render('index', { error,galeApiKey });
    } else {
        res.redirect('/home');
    }


});

baseRouter.get('/home', function (req, res) {
    if (typeof req.session.user === 'undefined' || typeof req.session.token === 'undefined') {
        res.redirect('/login')
    } else if (typeof req.session.patientId !== 'undefined') {
        res.redirect('/dashboard/:id')
    }
    else {
        const user = req.session.user
        const token = req.session.token;
        const starttime = req.session.sessionStartTime
        searchPatients(token, "").then((patients) => {
            logger.info("number of patients found" + patients.length)
            res.render("home", { error, user, patients, starttime,galeApiKey })

        }).catch((error) => {
            if (error.response.status == 401) {
                logout(req, res);
            }
        })
    }
});

baseRouter.get('/dashboard/:id', function (req, res) {
    if (typeof req.session.user === 'undefined' || typeof req.session.token === 'undefined' || typeof req.params.id === 'undefined') {
        res.redirect('/login');
    } else {
        if (typeof req.session.patientId === 'undefined') {
            req.session.patientId = req.params.id
            req.session.sessionStartTime = new Date().getTime();
            req.session.historyLoadedTime = new Date().getTime();;
        }

        logger.info(" select patient with id " + req.session.patientId)
        res.redirect('/dashboard')
    }
});

baseRouter.get('/dashboard', function (req, res) {
    if (typeof req.session.patientId === 'undefined' || typeof req.session.token === 'undefined') {
        res.redirect('/home');
    } else {

        const user = req.session.user;
        const token = req.session.token;
        const startTime = req.session.sessionStartTime
        const historyLoadedTime = req.session.historyLoadedTime
         getPatient(token, req.session.patientId).then((patient: Patient | undefined) => {
            if (patient) {
               addEncounter(token, patient, 'arrived').then((encounter: Encounter | undefined)=>{
                logger.info("Encounter:" + JSON.stringify(encounter))
                req.session.encounterId = encounter?.id
                getObservations(token, patient).then((observations: Observation[]) => {
                    const measurementHistory = toGaleMeasurments(observations)
                    req.session.historyLoadedTime = new Date().getTime();
                    res.render("dashboard", { error, user, patient, measurementHistory, startTime, historyLoadedTime,galeApiKey });
                })
            })
            }
        }).catch((error) => {
            if (error.response.status == 401) {
                logout(req, res);
            }
        })
    }
});

baseRouter.get('/createProfile', function (req, res) {
    if (typeof req.session.user === 'undefined' || typeof req.session.token === 'undefined') {
        res.redirect('/login');
    } else {
        const user = req.session.user;
        req.session.patientId = undefined
        res.render("createProfile", { error, user,galeApiKey });
    }
});

baseRouter.get('/intake', function (req, res) {
    if (typeof req.session.user === 'undefined' || typeof req.session.token === 'undefined' || typeof req.session.patientId === 'undefined') {
        res.redirect('/login');
    } else {
        const token = req.session.token;
        const user = req.session.user;
        getPatient(token, req.session.patientId).then((patient: Patient | undefined) => {
             res.render("intake", { error, user, patient,galeApiKey ,startClinicName});
        }).catch((error) => {
            if (error.response.status == 401) {
                logout(req, res);
            }
        });
    }
});

baseRouter.get('/patientProfile', function (req, res) {
    if (typeof req.session.user === 'undefined' || typeof req.session.token === 'undefined' || typeof req.session.patientId === 'undefined') {
        res.redirect('/login');
    } else {
        const user = req.session.user;
        const startTime = req.session.sessionStartTime
        getPatient(req.session.token, req.session.patientId).then((patient: Patient | undefined) => {
            console.log(patient)
            res.render("patientProfile", { error, user, patient, startTime,galeApiKey });
        }).catch((error) => {
            if (error.response.status == 401) {
                res.redirect('/logout')
            }
        });
    }
});

baseRouter.get('/visitSummary', function (req, res) {
    if (typeof req.session.user === 'undefined' || typeof req.session.token === 'undefined' || typeof req.session.patientId === 'undefined') {
        res.redirect('/login')
    } else {
        const user = req.session.user;
        const token = req.session.token;
        const startTime = req.session.sessionStartTime
        const historyLoadedTime = req.session.historyLoadedTime
        logger.info(' user ${user}' + token);
        getPatient(token, req.session.patientId).then((patient: Patient | undefined) => {
            console.log(patient)
            res.render("visitSummary", { user, patient, startTime, historyLoadedTime,galeApiKey });
        }).catch((error) => {
            if (error.response.status == 401) {
                res.redirect('/logout')
            }
        });
    }
})

baseRouter.get('/', function (req, res) {
    if (typeof req.session.user !== undefined) {
        res.redirect('/login');
    } else {
        res.redirect('/home');
    }
});


export default baseRouter;
