
function getIntake() {
    const bridge = window.bridge;
    console.log(bridge.getIntake())
}

function uploadObservations(observations) {
    const startTime = window.startTime
    observations.forEach((observation) => {
        const bridge = window.bridge;
        if (observation.timecomparitor < startTime){
        switch (observation.type) {
            case "Weight Scale": {
                bridge.addWeightScaleData(observation)
                break;
            }
            case "Thermometer": {
                bridge.addThermometerData(observation)
                break;
            }
            case "Glucometer": {
                bridge.addGlucometerData(observation)
                break;
            }
            case "Blood Pressure Monitor": {
                bridge.addBloodPressureData(observation)
                break;
            } case "Pulse Oximeter": {
                bridge.addPulseOxData(observation)
                break;
            }
        }
    }
    window.historyTime = new Date().getTime();

    })

}

function saveObservations() {
    const startTime = window.startTime;
    const bridge = window.bridge;
    var historyTime = window.historyTime
    if (!historyTime) {
        historyTime = startTime
    }
    bridge.getSensorData(historyTime).then((currentData) => {
        enableSpinner();
        Http.Post("/data/summary", currentData).then((response) => {
            console.log("upload response " + JSON.stringify(response))
            disableSpinner();
            window.historyTime = new Date().getTime();
            navigateToDashboard();
        }).catch((error) => {
            console.log(error);
            disableSpinner();
        });
    })

}

var Http = (function () {
    // Setup request for json
    var getOptions = function (verb, data) {
        var options = {
            dataType: "json",
            method: verb,
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            }
        };
        if (data) {
            options.body = JSON.stringify(data);
        }
        return options;
    }
    // Set Http methods
    return {
        Get: function (path) {
            return fetch(path, getOptions("GET"))
        },
        Post: function (path, data) {
            return fetch(path, getOptions("POST", data));
        },
        Put: function (path, data) {
            return fetch(path, getOptions("PUT", data));
        },
        Delete: function (path) {
            return fetch(path, getOptions("DELETE"));
        }
    };
})();

function logout() {
    enableSpinner();
    window.location.href = '/auth/logout'
}
var isVisit = false;
function startVisit() {
    isVisit = true;
}

function navigateToCreateProfile() {
    enableSpinner();
    window.location.href = "/createProfile"
}

function navigateToSelectPatient() {
    const bridge = window.bridge;
    bridge.endSession().then(() => {
        console.log("switch location to endPatient")
        window.location.href = "/auth/endPatientSession"
    });

    enableSpinner();


}

function navigateToVisitSummary() {
    enableSpinner();
    window.location.href = `/visitSummary`

}

function navigateToPatientProfile() {
    enableSpinner();
    window.location.href = `/patientProfile`
}


function navigateToDashboard(id) {
    enableSpinner();
    // if id is not defined go to current patient
    if (id === 'undefined') {
        window.location.href = `/dashboard`
    } else {
        window.location.href = `/dashboard/` + id
    }
}

function navigateToIntake() {
    enableSpinner();
    window.location.href = `/intake`
}

function enableSpinner() {
    $('#spinner').show();
}
function disableSpinner() {
    $('#spinner').hide();
}


