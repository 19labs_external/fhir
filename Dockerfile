FROM node:lts-gallium 
EXPOSE 4008
WORKDIR /var/www/app
COPY build.ts package.json ./yarn.lock ./tsconfig.json ./
COPY src ./src
RUN yarn && yarn build

ENTRYPOINT [ "yarn" ]
CMD ["start:dev"]