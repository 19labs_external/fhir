# README #

This is an example of using the Gale Wellness apis and custom Dom elements to integate with an external FHIR base EHR deployed in a captive web page on the Gale device

### Dependencies ###

This application depends on haveing an API key issued by 19Labs, and having a clinic named "Kiosk Example Clinic"
The application uses a modified version of the FHIR-CLIENT-KIT
the project is a simple nodejs/express app using ejs as the view render engine

The build system depends on Yarn

to Build


* update the constants.js with your API key
* yarn install
* yarn build
* yarn start:dev  or yarn start

to deploy

Add the public url that the app is running on to the wellness url in the DMS configuration for your device on 
the 19Labs careprovider site (stg or prod)

this uses the latest version of the @19labs/gale-wellness npm package
* the package is available at https://www.npmjs.com/package/@19labs/gale-wellness
* the documentation is available at https://unpkg.com/@19labs/gale-wellness/docs/index.html


Test Information
The application currently runs against the 19Labs FHIR repository on AWS
user login informatio

The patient "Emily Smith" has test data installed

test Data is regularily cleaned from this installation



### Environment variables ###

this uses several environment variables as follows
* NODE_ENV= development
* START_CLINIC= "name of the clinic to launch visits"
* PORT=4008  "port to run on"
* GALE_API_KEY= "your api key"



### Who do I talk to? ###

* contact support@19labs.com for any assistance